import { AzureFunction, Context, HttpRequest } from "@azure/functions";

const httpTrigger: AzureFunction = async function(
  context: Context,
  req: HttpRequest
): Promise<void> {
  context.log("Running hello world trigger.");
  const msg = req.query.msg || (req.body && req.body.msg) || "Default hello world it is!";

  // You can also deserialize automagically
  // const dirties: Serializable[] =
  //   req.query.serializable || (req.body && req.body.serializable);

  try {
    let res = {
      status: 200 /* Defaults to 200 */,
      headers: {
        "Content-Type": "application/json"
      },
      body: msg
    };

    context.done(null, res);
  } catch (e) {
    context.done(null, {
      body: e
    });
  }
};

export default httpTrigger;
