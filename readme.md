To use this repo, first install the azure functions cli: https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local

Open a terminal - you should be able to run 'func'

Run:
- npm install 
- npm run build
- npm run start 

You should see the function app start up with your code. The node instance is setup here to listen on port 5858.

If you're using vscode, open that up and hit f5 to debug. VS code should attach to the node instance and you should be able to step through your code.